from django.urls import path
from .views import list_all_todolists, todo_list_detail, todo_list_create, todo_list_update

urlpatterns = [
    path("", list_all_todolists, name="todo_list_list"),
    path ("<int:id>/", todo_list_detail, name="todo_list_detail" ),
    path("create/", todo_list_create, name="todo_list_create"),
    path("<int:id>edit/", todo_list_update, name="todo_list_update"),
]
