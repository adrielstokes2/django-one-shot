from django.forms import ModelForm
from todos.models import TodoList
from todos.models import TodoItem



class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = {
            "name",
        }
