from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404
from todos.models import TodoList
from todos.forms import TodoListForm
#from todos.forms import ListForm
# Create your views here.


def list_all_todolists(request):
    todolists = TodoList.objects.all()
    context = {
        "todolists": todolists,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo_list_detail,
    }
    return render(request, "todos/detail.html", context)




def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            to_do_list_instance = form.save()
            return redirect("todo_list_detail", id=to_do_list_instance.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request,"todos/create.html", context)




def todo_list_update(request, id):
    todos = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todos)
        if form.is_valid():
            new_list = form.save()
            return redirect("todo_list_detail", id=new_list.id)
    else:
        form = TodoListForm(instance=todos)

    context = {
        "form": form,
        "list_all_todolists": todos
    }
    return render(request, "todos/edit.html", context)
